// SPDX-License-Identifier: GPL-2.0-only
use std::process::exit;
use std::{io, iter};

fn main() {
    let mut winkey = String::new();
    println!("Enter Win95 Key:");
    io::stdin()
        .read_line(&mut winkey)
        .expect("Failed to read line");
    winkey = winkey.trim().parse().unwrap();
    if winkey.len() == 11 {
        println!("CD Key detected.");
        if cdkey_validate(winkey) != "Valid CD Key" {
            exit(1);
        } else {
            exit(0);
        }
    } else if winkey.len() == 23 {
        if oemkey_validate(winkey) != "Valid OEM Key" {
            exit(1);
        } else {
            exit(0);
        }
    } else {
        println!("Input was not 11 characters long. The format for a win95 key is: AAA-BBBBBBB");
        exit(1);
    }
}

fn cdkey_validate(cdkey: String) -> &'static str {
    let keyvec: Vec<&str> = cdkey.split('-').collect();
    let tri = keyvec[0].parse::<i16>().unwrap();
    let sept = keyvec[1].parse::<i32>().unwrap();
    if test_spam_cd(tri) == "Invalid Tri" {
        "Invalid Tri"
    } else {
        if sept_validate(sept) == "Invalid Key" {
            "Invalid CD Key"
        } else {
            println!("Valid CD Key");
            "Valid CD Key"
        }
    }
}

fn test_spam_cd(tri: i16) -> &'static str {
    if tri == 333 {
        println!("Invalid CD Key");
        "Invalid Tri"
    } else if tri == 444 {
        println!("Invalid CD Key");
        "Invalid Tri"
    } else if tri == 555 {
        println!("Invalid CD Key");
        "Invalid Tri"
    } else if tri == 666 {
        println!("Invalid CD Key");
        "Invalid Tri"
    } else if tri == 777 {
        println!("Invalid CD Key");
        "Invalid Tri"
    } else if tri == 888 {
        println!("Invalid CD Key");
        "Invalid Tri"
    } else if tri == 999 {
        println!("Invalid CD Key");
        "Invalid Tri"
    } else {
        "Valid Tri"
    }
}

fn sept_validate(sept: i32) -> &'static str {
    let sum: i32 = iter::successors(
        Some(sept),
        |&num| if num >= 10 { Some(num / 10) } else { None },
    )
    .map(|num| num % 10)
    .sum();
    return if sum % 7 != 0 {
        "Invalid Key"
    } else {
        "Valid Key"
    };
}

fn oemkey_validate(oemkey: String) -> &'static str {
    let keyvec: Vec<&str> = oemkey.split('-').collect();
    let quintalpha = keyvec[0];
    let oemstr = keyvec[1];
    let sept = keyvec[2].parse::<i32>().unwrap();
    let septstring = keyvec[2];
    let quintbeta = keyvec[3];
    if oemkey_quintalpha_validate(quintalpha) != "Quint Alpha Valid" {
        println!("Invalid OEM Key - Quint Alpha");
        "Invalid OEM Key"
    } else {
        if oemstr != "OEM" {
            println!("Invalid OEM Key - OEM String");
            "Invalid OEM Key"
        } else {
            if &septstring[0..0] == "0" {
                println!("Invalid OEM Key - Sept");
                "Invalid OEM Key"
            } else {
                if sept_validate(sept) == "Invalid Key" {
                    println!("Invalid OEM Key - Sept");
                    "Invalid OEM Key"
                } else {
                    if quintbeta.parse::<i32>().is_ok() {
                        println!("Valid OEM Key");
                        "Valid OEM Key"
                    } else {
                        println!("Invalid OEM Key - Quint Beta");
                        "Invalid OEM Key"
                    }
                }
            }
        }
    }
}

fn oemkey_quintalpha_validate(quint: &str) -> &'static str {
    let day = &quint[0..3].parse::<i32>().unwrap();
    let year = &quint[3..5].parse::<i32>().unwrap();
    if day >= &1 {
        if day <= &366 {
            if year == &95 {
                "Quint Alpha Valid"
            } else if year == &00 {
                "Quint Alpha Valid"
            } else if year == &01 {
                "Quint Alpha Valid"
            } else if year == &02 {
                "Quint Alpha Valid"
            } else {
                "Quint Alpha Invalid"
            }
        } else {
            "Quint Alpha Invalid"
        }
    } else {
        "Quint Alpha Invalid"
    }
}
